<?php
/**
 * @package MoneyChanger
 */
/*
Plugin Name: MoneyChanger by e-innovate
Plugin URI: https://rio.my.id
Description: Custom plugin for https://www.carpediemrome.com/
Version: 1.0
Author: Rio Bahtiar
Author URI: https://rio.my.id
License: GPLv2 or later
Text Domain: moneychanger
*/

define( 'MoneyChanger__PLUGIN_DIR', plugin_dir_path( __FILE__ ) );

require_once( MoneyChanger__PLUGIN_DIR . 'util.php' );
require_once( MoneyChanger__PLUGIN_DIR . 'fetcher.php' );
require_once( MoneyChanger__PLUGIN_DIR . 'script.php' );


// If plugin Activated, than fetch API for first time

register_activation_hook( __FILE__, 'mc_plugin_activate' );
function mc_plugin_activate() {
	mc_get_rates();
}

// Remove Scheduler if Plugin Deactivate
register_deactivation_hook( __FILE__, 'mc_deactivate_plugin' );

function mc_deactivate_plugin() {
	$timestamp = wp_next_scheduled( 'mc_get_rates_hook' );
	wp_unschedule_event( $timestamp, 'mc_get_rates_hook' );
}


// Setup Cron
if ( ! wp_next_scheduled( 'mc_get_rates_hook' ) ) {
	wp_schedule_event( time(), 'hourly', 'mc_get_rates_hook' );
}
add_action( 'mc_get_rates_hook', 'mc_get_rates' );
