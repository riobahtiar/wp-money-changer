<?php

/**
 * Check if data is valid JSON
 *
 * @param $string
 *
 * @return bool
 */
function mc_is_json( $string ) {
	json_decode( $string );

	return ( json_last_error() == JSON_ERROR_NONE );
}
