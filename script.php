<?php
function mc_load_script_header() {
	$rates = mc_get_rates();
	?>
	<script>
		// init variable
		var rates = <?php echo $rates ?>;
		var rateAUD = Number((rates.rates.AUD).toFixed(2));
		var rateUSD = Number((rates.rates.USD).toFixed(2));
		var rateEUR = Number((rates.rates.EUR).toFixed(2));
		var McConvertSymbolTag = '.elementor-grid-item.ecs-post-loop.tours .premium-prefix-text span';
		var McConvertRateValue = '.elementor-grid-item.ecs-post-loop.tours ul.premium-fancy-text-items-wrapper li';
		var McSingleSymbolTag = '.single-tours span.uael-price-table-currency';
		var McSingleRateValue = '.single-tours span.uael-price-table-integer-part';
		var McLabelTag = '#McLabel';
		var McMenuDropdownTag = '#primary-menu #menu-item-1324';

		var TemplateMcMenu = '<li id="menu-item-rate" class="menu-item menu-item-has-children menu-item-rates" aria-haspopup="true">' +
			'		<a id="McLabel">GBP £</a>' +
			'		<button class="ast-menu-toggle" aria-expanded="false"><span class="screen-reader-text">AUD $</span>' +
			'		</button>' +
			'		<ul class="sub-menu">' +
			'			<li id="menu-item-gbp" class="menu-item sub-menu-rates menu-item-gbp">' +
			'				<a href="#" onclick="McSetDefault()">GBP £</a>' +
			'			</li>' +
			'			<li id="menu-item-eur" class="menu-item sub-menu-rates menu-item-eur">' +
			'				<a href="#" onclick="McSetEUR()">EUR €</a>' +
			'			</li>' +
			'			<li id="menu-item-usd" class="menu-item sub-menu-rates menu-item-usd">' +
			'				<a href="#" onclick="McSetUSD()">USD $</a>' +
			'			</li>' +
			'			<li id="menu-item-aud" class="menu-item sub-menu-rates menu-item-aud">' +
			'				<a href="#" onclick="McSetAUD()">AUD $</a>' +
			'			</li>' +
			'		</ul>' +
			'	</li>';


		function McChangeSymbol(TargetAttr, OldSymbol, NewSymbol) {
			jQuery(TargetAttr).each(function () {
				var text = jQuery(this).text();
				var textFinal = text.replace(OldSymbol, NewSymbol);
				jQuery(this).text(textFinal);
			});
		}

		function McCalculate(TargetAttr, RateOrder) {
			jQuery(TargetAttr).each(function () {
				let text = parseFloat(jQuery(this).data('mc'));
				let mn = 1;

				switch (RateOrder) {
					case 1:
						mn = text;
						break;
					case 2:
						mn = text * rateEUR;
						break;
					case 3:
						mn = text * rateUSD;
						break;
					case 4:
						mn = text * rateAUD;
						break;
					default:
						mn = text;
				}

				jQuery(this).text(Number((mn).toFixed(2)));
			});
		}


		// Action per rate

		function McSetDefault() {
			localStorage.setItem("McNavLabel", "GBP £");
			localStorage.setItem("McRateSymbol", "£");
			localStorage.setItem("McRateOrder", "1");


			McCalculate(McConvertRateValue, 1);
			McCalculate(McSingleRateValue, 1);

			McChangeSymbol(McLabelTag, "USD $", "GBP £");
			McChangeSymbol(McLabelTag, "AUD $", "GBP £");
			McChangeSymbol(McLabelTag, "EUR $", "GBP £");
			McChangeSymbol(McConvertSymbolTag, "$", "£");
			McChangeSymbol(McConvertSymbolTag, "€", "£");
			McChangeSymbol(McSingleSymbolTag, "$", "£");
			McChangeSymbol(McSingleSymbolTag, "€", "£");
		}

		function McSetEUR() {
			McChangeSymbol(McLabelTag, localStorage.McNavLabel, "EUR €");
			McChangeSymbol(McConvertSymbolTag, localStorage.McRateSymbol, "€");
			McChangeSymbol(McSingleSymbolTag, localStorage.McRateSymbol, "€");

			McCalculate(McConvertRateValue, 2);
			McCalculate(McSingleRateValue, 2);

			localStorage.setItem("McNavLabel", "EUR €");
			localStorage.setItem("McRateSymbol", "€");
			localStorage.setItem("McRateOrder", "2");
		}

		function McSetUSD() {
			McChangeSymbol(McLabelTag, localStorage.McNavLabel, "USD $");
			McChangeSymbol(McConvertSymbolTag, localStorage.McRateSymbol, "$");
			McChangeSymbol(McSingleSymbolTag, localStorage.McRateSymbol, "$");

			McCalculate(McConvertRateValue, 3);
			McCalculate(McSingleRateValue, 3);

			localStorage.setItem("McNavLabel", "USD $");
			localStorage.setItem("McRateSymbol", "$");
			localStorage.setItem("McRateOrder", "3");
		}

		function McSetAUD() {
			McChangeSymbol(McLabelTag, localStorage.McNavLabel, "AUD $");
			McChangeSymbol(McConvertSymbolTag, localStorage.McRateSymbol, "$");
			McChangeSymbol(McSingleSymbolTag, localStorage.McRateSymbol, "$");

			McCalculate(McConvertRateValue, 4);
			McCalculate(McSingleRateValue, 4);

			localStorage.setItem("McNavLabel", "AUD $");
			localStorage.setItem("McRateSymbol", "$");
			localStorage.setItem("McRateOrder", "4");
		}


	</script>
	<style>


		@media screen and (min-width: 900px) {
			#primary-menu li#menu-item-rate a {
				font-weight: bold;
				color: yellow;
			}

			#primary-menu li#menu-item-rate > ul.sub-menu {
				text-align: center;
				width: 100px;
			}
		}

	</style>
	<?php
}


add_action( 'wp_head', 'mc_load_script_header' );

function mc_load_script_footer() {
	?>

	<script>
		jQuery(McMenuDropdownTag).after(TemplateMcMenu);

		if (typeof localStorage.McRateOrder == 'undefined') {
			localStorage.setItem('McNavLabel', 'GBP £');
			localStorage.setItem('McRateSymbol', '£');
			localStorage.setItem('McRateOrder', '1');
		} else {
			McChangeSymbol(McLabelTag, 'GBP £', localStorage.McNavLabel);
			McChangeSymbol(McConvertSymbolTag, '£', localStorage.McRateSymbol);
			McChangeSymbol(McSingleSymbolTag, '£', localStorage.McRateSymbol);
		}

		jQuery(McConvertRateValue).each(function () {
			var McValElm = parseFloat(jQuery(this).text());
			jQuery(this).attr('data-mc', McValElm);
		});

		jQuery(McSingleRateValue).each(function () {
			var McValElm = parseFloat(jQuery(this).text());
			jQuery(this).attr('data-mc', McValElm);
		});

		McCalculate(McConvertRateValue, parseFloat(localStorage.McRateOrder));
		McCalculate(McSingleRateValue, parseFloat(localStorage.McRateOrder));
	</script>
	<?php
}

add_action( 'wp_footer', 'mc_load_script_footer' );
