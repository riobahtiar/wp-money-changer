<?php
/**
 * Fetch api
 */


/**
 * Wrapper for get rates and cached data in transient
 * @return mixed|string|null
 */

function mc_get_rates() {
	$prefix       = 'mc_data_';
	$param        = 'base=GBP&symbols=AUD,EUR,USD';
	$time_expired = 4 * HOUR_IN_SECONDS;
	$transient    = get_transient( $prefix . 'current_rates' );

	if ( ! empty( $transient ) ) {

		return $transient;
	} else {
		$data = mc_fetch_api( $param );
		set_transient( $prefix . 'current_rates', $data, $time_expired );

		return $data;
	}
}

/**
 * Fetch rates from exchange API
 *
 * @param $param
 *
 * @return string|null
 */
function mc_fetch_api( $param ) {
	$api_url  = 'https://api.exchangeratesapi.io/latest?' . $param;
	$response = wp_remote_get( $api_url );
	$result   = wp_remote_retrieve_body( $response );
	if ( mc_is_json( $result ) && ! is_wp_error( $result ) ) {
		return $result;
	} else {
		return null;
	}
}
